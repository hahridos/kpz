﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Threading;

namespace WinFormsApp11
{
    public partial class Form1 : Form
    {
        static string path = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName, "WriteText.txt");
        private int count = 8;
        private new int Top = 10;
        
        private new int Left = 10;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Invalidate();
            string comm = InputDataBox.Text;
            if (!(comm.Contains("if") || comm.Contains("input()") || comm.Contains("=") || comm.Contains("print")))
            {
                return;
            }
            //Panel CrtPanel = new Panel();
            Button buttonToWrite = new Button();
            Panel SecondPanel = new Panel();
            buttonToWrite.Text = comm;
            //CrtPanel.Size = new Size(200, 100);
            SecondPanel.Size = new Size(200, 100);
            //CrtPanel.Location = new Point(Left, Top);
            SecondPanel.Location = new Point(Left+5, Top+5);
            buttonToWrite.Top = 35;
            buttonToWrite.Left = 55;
            //CrtPanel.BackColor = Color.Black;
            SecondPanel.BackColor = Color.Gray;
            buttonToWrite.BackColor = Color.White;
            SecondPanel.Controls.Add(buttonToWrite);
            this.Controls.Add(SecondPanel);
            //this.Controls.Add(CrtPanel);
            

            Top = Top + 110;
            count++;
            if (!File.Exists(path))
            {
                using (StreamWriter writer = File.CreateText(path)) { }
            }

            using (StreamWriter w = File.AppendText(path))
            {
                comm = Regex.Replace(comm, @"\s+", "");
                if (comm.StartsWith("if") && comm.EndsWith("{"))
                {
                    comm = comm.Replace("{", "\r\n{");
                }
                if (comm.EndsWith("}"))
                {
                    comm = comm.Replace("}", "\r\n}\r\n");
                }
                if (comm == "}") { }
                w.WriteLine(comm);
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {

            if(!(Top-110 < 0))
            {
                Top = Top - 110;
            }
            if (count > 8)
            {
                //Console.WriteLine(this.Controls[count].Controls[0].Text);
                if ((this.Controls[count].Controls[0].Text).Contains("}"))
                {
                    int con = (this.Controls[count].Controls[0].Text).Count(f => f == '}');
                    while (con != 0)
                    {
                        if ((this.Controls[count].Controls[0].Text).Contains("if"))
                        {
                            con--;
                        }
                        if (!(Top - 110 < 0))
                        {
                            Top = Top - 110;
                        }
                        this.Controls.RemoveAt(count);
                        count--;

                    }
                    //if ((this.Controls[count].Controls[0].Text).Contains("if"))
                    //{
                    //    if (!(Top - 110 < 0))
                    //    {
                    //        Top = Top - 110;
                    //    }
                    //    this.Controls.RemoveAt(count);
                    //    count--;
                    //}
                }
                else
                {
                    this.Controls.RemoveAt(count);
                    count--;
                }

            }

            string lines = System.IO.File.ReadAllText(path);
            string[] tokens = lines.Split("\r\n");
            System.IO.File.WriteAllText(path, string.Empty);
            for (int i=0; i < tokens.Length-1; i++)
            {
                if (tokens[i + 1] == "")
                {
                    break;
                }
                if (tokens[i].Contains("if") && tokens[i + 2] == "")
                {
                    break;
                }
                if (tokens[i].Contains("if"))
                {
                    int coutS = 1;
                    int j = i+2;
                    while(coutS != 0)
                    {
                        if (tokens[j] == "{")
                            coutS++;
                        if (tokens[j] == "}")
                            coutS--;
                        j++;
                    }
                    if (tokens[j] == "")
                    {
                        i = j;
                        break;
                    }
                    else
                    {
                        using (StreamWriter w = File.AppendText(path))
                        {
                            w.WriteLine(tokens[i]);
                        }
                        continue;
                    }
                }

                else
                {
                    using (StreamWriter w = File.AppendText(path))
                    {
                        w.WriteLine(tokens[i]);
                    }
                }
            }
            this.Invalidate();

        }
        private void button3_Click(object sender, EventArgs e)
        {
            Run();
        }
        public void Run()
        {
            Dictionary<string, string> variables = new Dictionary<string, string>();
            string contents = File.ReadAllText(path);
            string[] tokens = contents.Split("\r\n");
            bool Pass = false;
            foreach (var c in tokens)
            {
                //if Brakets
                if (Pass == true)
                {
                    continue;
                }
                if (c == "}")
                {
                    Pass = false;
                }
                //variables
                if (c.Contains("="))
                {
                    string[] splited = c.Split("=");
                    string getVar;
                    try
                    {
                        if (variables.ContainsKey(splited[1]))
                        {
                            getVar = GetRoot(variables, splited[1]);
                        }
                        else
                        {
                            getVar = splited[1];
                        }
                        variables.Add(splited[0], getVar);
                    }
                    catch (System.ArgumentException)
                    {
                        variables[splited[0]] = splited[1];
                    }
                }
                //print
                if (c.StartsWith("print(") && c.EndsWith(")"))
                {
                    string result = GetBetween(c, "print(", ")");

                    if (variables.ContainsKey(result))
                    {
                        result = GetRoot(variables, result) + "\n";
                        Console.WriteLine(result);
                    }
                    else
                    {
                        result = result + "\n";
                        Console.WriteLine(result);
                    }

                }
                //input
                if (c.Contains("=input()"))
                {
                    string[] splited = c.Split("=");
                    string ToPass = Console.ReadLine();
                    try
                    {
                        variables.Add(splited[0], ToPass);
                    }
                    catch (System.ArgumentException)
                    {
                        variables[splited[0]] = ToPass;
                    }
                }
                //if
                if (c.StartsWith("if(") && c.EndsWith(")"))
                {
                    string ToSpit = GetBetween(c, "if(", ")");
                    if (c.Contains("=="))
                    {
                        string[] splited = ToSpit.Split("==");
                        if ((int.TryParse(splited[0], out int j)) && (int.TryParse(splited[1], out int k)))
                        {
                            if (Int32.Parse(splited[0]) == Int32.Parse(splited[1]))
                            {
                                Pass = false;
                            }
                            else
                            {
                                Pass = true;
                            }
                        }
                        if ((int.TryParse(splited[0], out j)) && variables.ContainsKey(splited[1]))
                        {
                            string getVar1 = GetRoot(variables, splited[1]);
                            if (Int32.Parse(splited[0]) == Int32.Parse(getVar1))
                            {
                                Pass = false;
                            }
                            else
                            {
                                Pass = true;
                            }
                        }
                        if ((variables.ContainsKey(splited[0])) && (int.TryParse(splited[1], out k)))
                        {
                            string getVar0 = GetRoot(variables, splited[0]);
                            if (Int32.Parse(getVar0) == Int32.Parse(splited[1]))
                            {
                                Pass = false;
                            }
                            else
                            {
                                Pass = true;
                            }
                        }
                        else
                        {
                            if (variables.ContainsKey(splited[0]) && variables.ContainsKey(splited[1]))
                            {
                                string getVar0 = GetRoot(variables, splited[0]);
                                string getVar1 = GetRoot(variables, splited[1]);
                                if ((int.TryParse(getVar0, out j)) && (int.TryParse(getVar1, out k)))
                                {
                                    if (Int32.Parse(getVar0) == Int32.Parse(getVar1))
                                    {
                                        Pass = false;
                                    }
                                    else
                                    {
                                        Pass = true;
                                    }
                                }
                                else
                                {
                                    Pass = true;
                                }
                            }
                            else
                            {
                                Pass = true;
                            }
                        }
                    }
                    if (c.Contains("<"))
                    {
                        string[] splited = ToSpit.Split("<");
                        if ((int.TryParse(splited[0], out int j)) && (int.TryParse(splited[1], out int k)))
                        {
                            if (Int32.Parse(splited[0]) < Int32.Parse(splited[1]))
                            {
                                Pass = false;
                            }
                            else
                            {
                                Pass = true;
                            }
                        }
                        if ((int.TryParse(splited[0], out j)) && variables.ContainsKey(splited[1]))
                        {
                            string getVar1 = GetRoot(variables, splited[1]);
                            if (Int32.Parse(splited[0]) < Int32.Parse(getVar1))
                            {
                                Pass = false;
                            }
                            else
                            {
                                Pass = true;
                            }
                        }
                        if ((variables.ContainsKey(splited[0])) && (int.TryParse(splited[1], out k)))
                        {
                            string getVar0 = GetRoot(variables, splited[0]);
                            if (Int32.Parse(getVar0) < Int32.Parse(splited[1]))
                            {
                                Pass = false;
                            }
                            else
                            {
                                Pass = true;
                            }
                        }
                        else
                        {
                            if (variables.ContainsKey(splited[0]) && variables.ContainsKey(splited[1]))
                            {
                                string getVar0 = GetRoot(variables, splited[0]);
                                string getVar1 = GetRoot(variables, splited[1]);
                                if ((int.TryParse(getVar0, out j)) && (int.TryParse(getVar1, out k)))
                                {
                                    if (Int32.Parse(getVar0) < Int32.Parse(getVar1))
                                    {
                                        Pass = false;
                                    }
                                    else
                                    {
                                        Pass = true;
                                    }
                                }
                                else
                                {
                                    Pass = true;
                                }
                            }
                            else
                            {
                                Pass = true;
                            }
                        }

                    }
                    if (c.Contains(">"))
                    {
                        string[] splited = ToSpit.Split(">");
                        if ((int.TryParse(splited[0], out int j)) && (int.TryParse(splited[1], out int k)))
                        {
                            if (Int32.Parse(splited[0]) > Int32.Parse(splited[1]))
                            {
                                Pass = false;
                            }
                            else
                            {
                                Pass = true;
                            }
                        }
                        if ((int.TryParse(splited[0], out j)) && variables.ContainsKey(splited[1]))
                        {
                            string getVar1 = GetRoot(variables, splited[1]);
                            if (Int32.Parse(splited[0]) > Int32.Parse(getVar1))
                            {
                                Pass = false;
                            }
                            else
                            {
                                Pass = true;
                            }
                        }
                        if ((variables.ContainsKey(splited[0])) && (int.TryParse(splited[1], out k)))
                        {
                            string getVar0 = GetRoot(variables, splited[0]);
                            if (Int32.Parse(getVar0) > Int32.Parse(splited[1]))
                            {
                                Pass = false;
                            }
                            else
                            {
                                Pass = true;
                            }
                        }
                        else
                        {
                            if (variables.ContainsKey(splited[0]) && variables.ContainsKey(splited[1]))
                            {
                                string getVar0 = GetRoot(variables, splited[0]);
                                string getVar1 = GetRoot(variables, splited[1]);
                                if ((int.TryParse(getVar0, out j)) && (int.TryParse(getVar1, out k)))
                                {
                                    if (Int32.Parse(getVar0) > Int32.Parse(getVar1))
                                    {
                                        Pass = false;
                                    }
                                    else
                                    {
                                        Pass = true;
                                    }
                                }
                                else
                                {
                                    Pass = true;
                                }
                            }
                            else
                            {
                                Pass = true;
                            }
                        }
                    }
                }
            }
        }

        static public string GetRoot(Dictionary<string, string> variables, string res)
        {
            while (true)
            {
                if (variables.ContainsKey(res))
                {
                    res = variables[res];
                }
                else
                {
                    break;
                }

            }
            return res;
        }

        static public string GetBetween(string c, string From, string To)
        {
            int pFrom = c.IndexOf(From) + From.Length;
            int pTo = c.LastIndexOf(To);
            string res = c.Substring(pFrom, pTo - pFrom);
            return res;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (int.TryParse(textBox1.Text, out int j))
            {
                for (int i = 0; i < Int32.Parse(textBox1.Text); i++)
                {
                    Thread myThread = new Thread(() => Run());
                    myThread.Start();
                }
            }

        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Panel tempbut = new Panel();
            var ListOfPanels = this.Controls.OfType<Panel>();
            base.OnPaint(e);
            Pen pen = new Pen(Color.Black, 3);
            int i = 0;
            foreach (Panel each in ListOfPanels)
            {

                if (i % 2 == 0) {
                    if (each.Name != "button1" && each.Name != "button2")
                    {
                        Point tpPoinFst = new Point();
                        tpPoinFst.X = each.Location.X + 100;
                        tpPoinFst.Y = each.Location.Y + 100;
                        Pen cvet = new Pen(Color.Black);
                        cvet.Width = 10;
                        using (var blackPen = new Pen(Color.Black, 3))
                            e.Graphics.DrawLine(cvet, tpPoinFst, new Point(tempbut.Location.X + 100, tempbut.Location.Y + 100));
                        tempbut = each;
                    }
                }
                i++;
            }

        }
        private void Form1_Paint(object sender, PaintEventArgs e)
        {}

        private void button6_Click(object sender, EventArgs e)
        {
            this.Invalidate();
        }
    }
}
